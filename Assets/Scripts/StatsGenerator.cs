﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  A <see langword="static"/> class with methods (functions) for initialising or randomising the stats class.
///  
/// TODO:
///     Initialise a stats instance with generated starting stats
///     Handle the assignment of extra points or xp into an existing stats of a character
///         - this is expected to be used by NPCs leveling up to match the player.
/// </summary>
public static class StatsGenerator
{
    public static void InitialStats(Stats stats)
    {
        Debug.Log("Initialise stats have been called");
        // Initialise our default level.
        stats.level = 1;
        // create a temporary variable that holds the total amount of points we can assign
        int somePoints = 10;
        stats.luck = 6;
        somePoints -= 6;
        // from this pool we are going to assign random points to each stat: Style, Luck & Rhythm
        // Once we assign points we want to remove them from the total points pool
    }

    public static void AssignUnusedPoints(Stats stats, int points)
    {
        Debug.Log("Assigned Unused Points has been Called");
        // Take those points and randomly assign them to stats.luck, style and rhythm
        // Once stats are assigned we need to take them away from our points
        // If we assign points randomly, we need to check to see if any points are left before we assign them.
        // Use IF statments
    }
}
