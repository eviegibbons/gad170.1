﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class with method (function) to determine the outcome of a dance battle based on the player and NPC that are 
///     dancing off against each other.
///     
/// TODO:
///     Battle needs to use stats and random to determine the winner of the dance off
///       - outcome value to be a float value between 1 and negative 1. 1 being the biggest possible player win over NPC, 
///         through to -1 being the most decimating defeat of the player possible.
/// </summary>
public static class BattleHandler
{


    public static void Battle(BattleEventData data)
    {
        //This needs to be replaced with some actual battle logic, at present 
        // we just award the maximum possible win to the player
        float outcome = 0;

        if (data.player.luck >= data.npc.luck)
        {
            outcome += 0.25f;

            Debug.Log(outcome);
        }
        else
        {
            outcome -= 0.25f;
        }

        if (data.player.style >= data.npc.style)
        {
            outcome += 1f;
        }
        else
        {
            outcome -= 1f;
        }

        if (data.player.rhythm >= data.npc.rhythm)
        {
            outcome += 0.5f;
        }
        else
        {
            outcome -= 0.5f;
        }

        var results = new BattleResultEventData(data.player, data.npc, outcome);

        if (outcome > 0)
        {
            data.player.xp += outcome;
        }

        GameEvents.FinishedBattle(results);
    }
}
