﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines the dance stats of a character.
/// 
/// TODO:
///     Nothing, but this class may be a good place to put some helper fuctions when dealing with xp to level conversion and the like.
/// </summary>
public class Stats : MonoBehaviour
{
    //[HideInInspector]
    public int level;
    //[HideInInspector]
    public float xp;
    public int xpCapPerLevel;
        
    //[HideInInspector]
    public int style, luck, rhythm;

    public void Update()
    {
        if (xp >= xpCapPerLevel)
        {
            LevelUp(xpCapPerLevel);
            xp = 0;
            xpCapPerLevel += 5;
        }
    }
    private void Awake()
    {
        //assign initial stats
        StatsGenerator.InitialStats(this);
    }
  
    public void LevelUp(int XpToAdd)
    {
        Debug.Log("level up was called and we added this much xp!" + XpToAdd);
        /*
         * first we check to see if the XP is greater than 0 otherwise what's the point
         * Add the XP to the current XP
         * Then check if we have levelled up
         * We are then going to increment the level by 1
         * Call the statsgenerator to assign any unused points
         */

        StatsGenerator.AssignUnusedPoints(this, 0);
        GameEvents.PlayerLevelUp(level);
    }
}

